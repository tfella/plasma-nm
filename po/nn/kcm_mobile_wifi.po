# Translation of kcm_mobile_wifi to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2019, 2020.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-28 00:19+0000\n"
"PO-Revision-Date: 2021-12-15 12:49+0100\n"
"Last-Translator: Oystein Steffensen-Alvaervik <ystein@posteo.net>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#: package/contents/ui/ConnectDialog.qml:136
#, kde-format
msgid "Invalid input."
msgstr "Ugyldige inndata."

#: package/contents/ui/ConnectionItemDelegate.qml:107
#, kde-format
msgid "Connect to"
msgstr "Kopla til"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Wi-Fi is disabled"
msgstr "Wi-Fi er slått av"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Enable"
msgstr "Ta i bruk"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Disable Wi-Fi"
msgstr "Slå av Wi-Fi"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "Add Custom Connection"
msgstr "Legg til tilpassa tilkopling"

#: package/contents/ui/main.qml:108
#, kde-format
msgid "Show Saved Connections"
msgstr "Vis lagra tilkoplingar"

#: package/contents/ui/NetworkSettings.qml:14
#, kde-format
msgid "Add new Connection"
msgstr "Legg til ny tilkopling"

#: package/contents/ui/NetworkSettings.qml:34
#, kde-format
msgid "Save"
msgstr "Lagra"

#: package/contents/ui/NetworkSettings.qml:44
#, kde-format
msgid "General"
msgstr "Generelt"

#: package/contents/ui/NetworkSettings.qml:49
#, kde-format
msgid "SSID:"
msgstr "SSID:"

#: package/contents/ui/NetworkSettings.qml:58
#, kde-format
msgid "Hidden Network:"
msgstr "Ikkje-synleg nettverk:"

#: package/contents/ui/NetworkSettings.qml:64
#, kde-format
msgid "Security"
msgstr "Tryggleik"

#: package/contents/ui/NetworkSettings.qml:70
#, kde-format
msgid "Security type:"
msgstr "Tryggleikstype:"

#: package/contents/ui/NetworkSettings.qml:79
#, kde-format
msgid "None"
msgstr "Ingen"

#: package/contents/ui/NetworkSettings.qml:80
#, kde-format
msgid "WEP Key"
msgstr "WEP-nøkkel"

#: package/contents/ui/NetworkSettings.qml:81
#, kde-format
msgid "Dynamic WEP"
msgstr "Dynamisk WEP"

#: package/contents/ui/NetworkSettings.qml:82
#, kde-format
msgid "WPA/WPA2 Personal"
msgstr "WPA/WPA2 personleg"

#: package/contents/ui/NetworkSettings.qml:83
#, kde-format
msgid "WPA/WPA2 Enterprise"
msgstr "WPA/WPA2 firma"

#: package/contents/ui/NetworkSettings.qml:108
#, kde-format
msgid "Password:"
msgstr "Passord:"

#: package/contents/ui/NetworkSettings.qml:116
#, kde-format
msgid "Authentication:"
msgstr "Autentisering:"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "TLS"
msgstr "TLS"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "LEAP"
msgstr "LEAP"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "FAST"
msgstr "FAST"

#: package/contents/ui/NetworkSettings.qml:120
#, kde-format
msgid "Tunneled TLS"
msgstr "Tunellert TLS (TTLS)"

#: package/contents/ui/NetworkSettings.qml:121
#, kde-format
msgid "Protected EAP"
msgstr "Verna EAP (PEAP)"

#: package/contents/ui/NetworkSettings.qml:130
#, kde-format
msgid "IP settings"
msgstr "IP-innstillingar"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Automatic"
msgstr "Automatisk"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Manual"
msgstr "Manuell"

#: package/contents/ui/NetworkSettings.qml:146
#, kde-format
msgid "IP Address:"
msgstr "IP-adresse:"

#: package/contents/ui/NetworkSettings.qml:158
#, kde-format
msgid "Gateway:"
msgstr "Portnar:"

#: package/contents/ui/NetworkSettings.qml:170
#, kde-format
msgid "Network prefix length:"
msgstr "Prefikslengd for nettverk:"

#: package/contents/ui/NetworkSettings.qml:183
#, kde-format
msgid "DNS:"
msgstr "DNS:"

#: package/contents/ui/PasswordField.qml:13
#, kde-format
msgid "Password…"
msgstr "Passord …"

#: wifisettings.cpp:32
#, kde-format
msgid "Wi-Fi networks"
msgstr "Wi-Fi-nettverk"

#: wifisettings.cpp:33
#, kde-format
msgid "Martin Kacej"
msgstr "Martin Kacej"
