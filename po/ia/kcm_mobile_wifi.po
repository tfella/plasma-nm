# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
#
# giovanni <g.sora@tiscali.it>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-28 00:19+0000\n"
"PO-Revision-Date: 2021-07-29 22:53+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Giovanni Sora"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "g.sora@tiscali.it"

#: package/contents/ui/ConnectDialog.qml:136
#, kde-format
msgid "Invalid input."
msgstr "Entrata invalide."

#: package/contents/ui/ConnectionItemDelegate.qml:107
#, kde-format
msgid "Connect to"
msgstr "Connecte a"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Wi-Fi is disabled"
msgstr "Wi-Fi es dishabilitate"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Enable"
msgstr "Habilita"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Disable Wi-Fi"
msgstr "Disactivar Wi-Fi"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "Add Custom Connection"
msgstr "Adde connexion personalisate"

#: package/contents/ui/main.qml:108
#, kde-format
msgid "Show Saved Connections"
msgstr "Monstra connexiones salveguardate"

#: package/contents/ui/NetworkSettings.qml:14
#, kde-format
msgid "Add new Connection"
msgstr "Adde nove connexion"

#: package/contents/ui/NetworkSettings.qml:34
#, kde-format
msgid "Save"
msgstr "Salveguarda"

#: package/contents/ui/NetworkSettings.qml:44
#, kde-format
msgid "General"
msgstr "General"

#: package/contents/ui/NetworkSettings.qml:49
#, kde-format
msgid "SSID:"
msgstr "SSID:"

#: package/contents/ui/NetworkSettings.qml:58
#, kde-format
msgid "Hidden Network:"
msgstr "Rete celate:"

#: package/contents/ui/NetworkSettings.qml:64
#, kde-format
msgid "Security"
msgstr "Securitate"

#: package/contents/ui/NetworkSettings.qml:70
#, kde-format
msgid "Security type:"
msgstr "Typo de securitate:"

#: package/contents/ui/NetworkSettings.qml:79
#, kde-format
msgid "None"
msgstr "Necun"

#: package/contents/ui/NetworkSettings.qml:80
#, kde-format
msgid "WEP Key"
msgstr "Clave de WEP"

#: package/contents/ui/NetworkSettings.qml:81
#, kde-format
msgid "Dynamic WEP"
msgstr "WEP dynamic"

#: package/contents/ui/NetworkSettings.qml:82
#, kde-format
msgid "WPA/WPA2 Personal"
msgstr "WPA/WPA2 Personal"

#: package/contents/ui/NetworkSettings.qml:83
#, kde-format
msgid "WPA/WPA2 Enterprise"
msgstr "WéA/WPA2 Interprisa"

#: package/contents/ui/NetworkSettings.qml:108
#, kde-format
msgid "Password:"
msgstr "Contrasigno:"

#: package/contents/ui/NetworkSettings.qml:116
#, kde-format
msgid "Authentication:"
msgstr "Authentication:"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "TLS"
msgstr "TLS"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "LEAP"
msgstr "LEAP"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "FAST"
msgstr "Rapide (FAST)"

#: package/contents/ui/NetworkSettings.qml:120
#, kde-format
msgid "Tunneled TLS"
msgstr "Tunneled TLS"

#: package/contents/ui/NetworkSettings.qml:121
#, kde-format
msgid "Protected EAP"
msgstr "EAP (PEAP) protegite"

#: package/contents/ui/NetworkSettings.qml:130
#, kde-format
msgid "IP settings"
msgstr "Preferentias de IP"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Automatic"
msgstr "Automatic"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Manual"
msgstr "Manual"

#: package/contents/ui/NetworkSettings.qml:146
#, kde-format
msgid "IP Address:"
msgstr "Adresse IP:"

#: package/contents/ui/NetworkSettings.qml:158
#, kde-format
msgid "Gateway:"
msgstr "Gateway:"

#: package/contents/ui/NetworkSettings.qml:170
#, kde-format
msgid "Network prefix length:"
msgstr "Longitude de prefixo de rete:"

#: package/contents/ui/NetworkSettings.qml:183
#, kde-format
msgid "DNS:"
msgstr "DNS:"

#: package/contents/ui/PasswordField.qml:13
#, kde-format
msgid "Password…"
msgstr "Contrasigno…"

#: wifisettings.cpp:32
#, kde-format
msgid "Wi-Fi networks"
msgstr "Retes Wi-Fi"

#: wifisettings.cpp:33
#, kde-format
msgid "Martin Kacej"
msgstr "Martin Kacej"

#~ msgctxt "@action:button"
#~ msgid "Cancel"
#~ msgstr "Cancella"

#~ msgctxt "@action:button"
#~ msgid "Done"
#~ msgstr "Facite"

#~ msgid "Password..."
#~ msgstr "Contrasigno..."

#~ msgid "Configure"
#~ msgstr "Configura"

#~ msgid "SSID"
#~ msgstr "SSID"

#~ msgid "(Unchanged)"
#~ msgstr "(Non modificate)"

#, fuzzy
#~| msgid "Delete connection "
#~ msgid "Delete connection %1 from device?"
#~ msgstr "Dele connexion "

#~ msgid "Delete"
#~ msgstr "Dele"

#~ msgid "Saved networks"
#~ msgstr "Retes salveguardate"

#~ msgid "Available networks"
#~ msgstr "Retes disponibile"

#~ msgid "DNS"
#~ msgstr "DNS"

#~ msgid "Wi-fi"
#~ msgstr "Wi-fi"
